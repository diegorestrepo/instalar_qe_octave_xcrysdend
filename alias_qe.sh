#!/bin/bash

# Escrito por Diego Andrés Restrepo Leal
# diegorestrepoleal@gmail.com

# Color de los mensajes
AZUL=$(tput setaf 6)
LIMPIAR=$(tput sgr 0)

echo
echo "$AZUL ======================================================================= $LIMPIAR"
echo "$AZUL = Ingrese el nombre del usuario en donde se ejecutaran los programas  = $LIMPIAR"
echo "$AZUL ======================================================================= $LIMPIAR"
echo
read usuario

echo "#Quantum-ESPRESSO: Inicio" >> /home/$usuario/.bashrc
echo "alias average.x='average.x_openmpi'" >> /home/$usuario/.bashrc
echo "alias bands.x='bands.x_openmpi'" >> /home/$usuario/.bashrc
echo "alias bgw2pw.x='bgw2pw.x_openmpi'" >> /home/$usuario/.bashrc
echo "alias bse_main.x='bse_main.x_openmpi'" >> /home/$usuario/.bashrc
echo "alias casino2upf.x='casino2upf.x_openmpi'" >> /home/$usuario/.bashrc
echo "alias cpmd2upf.x='cpmd2upf.x_openmpi'" >> /home/$usuario/.bashrc
echo "alias cppp.x='cppp.x_openmpi'" >> /home/$usuario/.bashrc
echo "alias cp.x='cp.x_openmpi'" >> /home/$usuario/.bashrc
echo "alias d3.x='d3.x_openmpi'" >> /home/$usuario/.bashrc
echo "alias dist.x='dist.x_openmpi'" >> /home/$usuario/.bashrc
echo "alias dos.x='dos.x_openmpi'" >> /home/$usuario/.bashrc
echo "alias dynmat.x='dynmat.x_openmpi'" >> /home/$usuario/.bashrc
echo "alias epsilon.x='epsilon.x_openmpi'" >> /home/$usuario/.bashrc
echo "alias ev.x='ev.x_openmpi'" >> /home/$usuario/.bashrc
echo "alias fd_ef.x='fd_ef.x_openmpi'" >> /home/$usuario/.bashrc
echo "alias fd_ifc.x='fd_ifc.x_openmpi'" >> /home/$usuario/.bashrc
echo "alias fd.x='fd.x_openmpi'" >> /home/$usuario/.bashrc
echo "alias fhi2upf.x='fhi2upf.x_openmpi'" >> /home/$usuario/.bashrc
echo "alias fpmd2upf.x='fpmd2upf.x_openmpi'" >> /home/$usuario/.bashrc
echo "alias fqha.x='fqha.x_openmpi'" >> /home/$usuario/.bashrc
echo "alias fs.x='fs.x_openmpi'" >> /home/$usuario/.bashrc
echo "alias generate_rVV10_kernel_table.x='generate_rVV10_kernel_table.x_openmpi'" >> /home/$usuario/.bashrc
echo "alias generate_vdW_kernel_table.x='generate_vdW_kernel_table.x_openmpi'" >> /home/$usuario/.bashrc
echo "alias gww_fit.x='gww_fit.x_openmpi'" >> /home/$usuario/.bashrc
echo "alias gww.x='gww.x_openmpi'" >> /home/$usuario/.bashrc
echo "alias head.x='head.x_openmpi'" >> /home/$usuario/.bashrc
echo "alias importexport_binary.x='importexport_binary.x_openmpi'" >> /home/$usuario/.bashrc
echo "alias initial_state.x_openmpi='initial_state.x_openmpi'" >> /home/$usuario/.bashrc
echo "alias interpolate.x='interpolate.x_openmpi'" >> /home/$usuario/.bashrc
echo "alias iotk='iotk_openmpi'" >> /home/$usuario/.bashrc
echo "alias iotk_print_kinds.x='iotk_print_kinds.x_openmpi'" >> /home/$usuario/.bashrc
echo "alias iotk.x='iotk.x_openmpi'" >> /home/$usuario/.bashrc
echo "alias kpoints.x='kpoints.x_openmpi'" >> /home/$usuario/.bashrc
echo "alias lambda.x='lambda.x_openmpi'" >> /home/$usuario/.bashrc
echo "alias ld1.x='ld1.x_openmpi'" >> /home/$usuario/.bashrc
echo "alias manycp.x='manycp.x_openmpi'" >> /home/$usuario/.bashrc
echo "alias manypw.x='manypw.x_openmpi'" >> /home/$usuario/.bashrc
echo "alias matdyn.x='matdyn.x_openmpi'" >> /home/$usuario/.bashrc
echo "alias molecularpdos.x='molecularpdos.x_openmpi'" >> /home/$usuario/.bashrc
echo "alias ncpp2upf.x='ncpp2upf.x_openmpi'" >> /home/$usuario/.bashrc
echo "alias neb.x='neb.x_openmpi'" >> /home/$usuario/.bashrc
echo "alias oldcp2upf.x='oldcp2upf.x_openmpi'" >> /home/$usuario/.bashrc
echo "alias path_interpolation.x='path_interpolation.x_openmpi'" >> /home/$usuario/.bashrc
echo "alias phcg.x='phcg.x_openmpi'" >> /home/$usuario/.bashrc
echo "alias ph.x='ph.x_openmpi'" >> /home/$usuario/.bashrc
echo "alias plan_avg.x='plan_avg.x_openmpi'" >> /home/$usuario/.bashrc
echo "alias plotband.x='plotband.x_openmpi'" >> /home/$usuario/.bashrc
echo "alias plotproj.x='plotproj.x_openmpi'" >> /home/$usuario/.bashrc
echo "alias plotrho.x='plotrho.x_openmpi'" >> /home/$usuario/.bashrc
echo "alias pmw.x='pmw.x_openmpi'" >> /home/$usuario/.bashrc
echo "alias pp.x='pp.x_openmpi'" >> /home/$usuario/.bashrc
echo "alias projwfc.x='projwfc.x_openmpi'" >> /home/$usuario/.bashrc
echo "alias pw2bgw.x='pw2bgw.x_openmpi'" >> /home/$usuario/.bashrc
echo "alias pw2gw.x='pw2gw.x_openmpi'" >> /home/$usuario/.bashrc
echo "alias pw2wannier90.x='pw2wannier90.x_openmpi'" >> /home/$usuario/.bashrc
echo "alias pw4gww.x='pw4gww.x_openmpi'" >> /home/$usuario/.bashrc
echo "alias pwcond.x='pwcond.x_openmpi'" >> /home/$usuario/.bashrc
echo "alias pw_export.x='pw_export.x_openmpi'" >> /home/$usuario/.bashrc
echo "alias pwi2xsf.x='pwi2xsf.x_openmpi'" >> /home/$usuario/.bashrc
echo "alias pw.x='pw.x_openmpi'" >> /home/$usuario/.bashrc
echo "alias q2qstar.x='q2qstar.x_openmpi'" >> /home/$usuario/.bashrc
echo "alias q2r.x='q2r.x_openmpi'" >> /home/$usuario/.bashrc
echo "alias q2trans_fd.x='q2trans_fd.x_openmpi'" >> /home/$usuario/.bashrc
echo "alias q2trans.x='q2trans.x_openmpi'" >> /home/$usuario/.bashrc
echo "alias read_upf_tofile.x='read_upf_tofile.x_openmpi'" >> /home/$usuario/.bashrc
echo "alias rrkj2upf.x='rrkj2upf.x_openmpi'" >> /home/$usuario/.bashrc
echo "alias spectra_manipulation.x='spectra_manipulation.x_openmpi'" >> /home/$usuario/.bashrc
echo "alias sumpdos.x='sumpdos.x_openmpi'" >> /home/$usuario/.bashrc
echo "alias turbo_davidson.x='turbo_davidson.x_openmpi'" >> /home/$usuario/.bashrc
echo "alias turbo_eels.x='turbo_eels.x_openmpi'" >> /home/$usuario/.bashrc
echo "alias turbo_lanczos.x='turbo_lanczos.x_openmpi'" >> /home/$usuario/.bashrc
echo "alias turbo_spectrum.x='turbo_spectrum.x_openmpi'" >> /home/$usuario/.bashrc
echo "alias upf2casino.x='upf2casino.x_openmpi'" >> /home/$usuario/.bashrc
echo "alias uspp2upf.x='uspp2upf.x_openmpi'" >> /home/$usuario/.bashrc
echo "alias vdb2upf.x='vdb2upf.x_openmpi'" >> /home/$usuario/.bashrc
echo "alias virtual.x='virtual.x_openmpi'" >> /home/$usuario/.bashrc
echo "alias wannier_ham.x='wannier_ham.x_openmpi'" >> /home/$usuario/.bashrc
echo "alias wannier_plot.x='wannier_plot.x_openmpi'" >> /home/$usuario/.bashrc
echo "alias wfck2r.x='wfck2r.x_openmpi'" >> /home/$usuario/.bashrc
echo "alias wfdd.x='wfdd.x_openmpi'" >> /home/$usuario/.bashrc
echo "alias xspectra.x='xspectra.x_openmpi'" >> /home/$usuario/.bashrc
echo "#Quantum-ESPRESSO: Fin" >> /home/$usuario/.bashrc

exit 0
