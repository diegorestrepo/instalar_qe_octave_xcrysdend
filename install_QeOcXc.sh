#!/bin/bash

# * ------------------------------------------------------------------------------------
# * "THE BEER-WARE LICENSE" (Revision 42):
# * <diegorestrepoleal@gmail.com> wrote this file. As long as you retain this notice you
# * can do whatever you want with this stuff. If we meet some day, and you think
# * this stuff is worth it, you can buy me a beer in return Diego Andrés Restrepo Leal.
# * ------------------------------------------------------------------------------------

# Limpiar pantalla
clear

# Color de los mensajes
AZUL=$(tput setaf 6)
VERDE=$(tput setaf 2)
ROJO=$(tput setaf 1)
LILA=$(tput setaf 5)
LIMPIAR=$(tput sgr 0)

debian ()
{
	echo
	echo "$AZUL Instalar build-essential $LIMPIAR"
	echo
	$GESTOR install -y build-essential

	echo
	echo "$AZUL Instalar LAPACK $LIMPIAR"
	echo
	$GESTOR install -y liblapack-dev

	echo
	echo "$AZUL Instalar BLAS $LIMPIAR"
	echo
	$GESTOR install -y libblas-dev

	echo
	echo "$AZUL Instalar FFTW y FFTW3 $LIMPIAR"
	echo
	$GESTOR install -y fftw-dev
	$GESTOR install -y fftw3-dev

	echo
	echo "$AZUL Instalar gfortran $LIMPIAR"
	echo
	$GESTOR install -y gfortran

	echo
	echo "$AZUL Instalar openmpi $LIMPIAR"
	echo
	$GESTOR install -y libopenmpi-dev

	echo
	echo "$AZUL Instalar Quantum-ESPRESSO $LIMPIAR"
	echo
	$GESTOR install -y quantum-espresso

	echo
	echo "$AZUL Instalar XCrySDen $LIMPIAR"
	echo
	$GESTOR install -y xcrysden

	echo
	echo "$AZUL Instalar octave $LIMPIAR"
	echo
	$GESTOR install -y octave

	echo
	echo "$AZUL Instalar nano $LIMPIAR"
	echo
	$GESTOR install -y nano

	echo
	echo "$AZUL Instalar tmux $LIMPIAR"
	echo
	$GESTOR install -y tmux
}


redhat ()
{

	echo
	echo "$AZUL ======================================================================= $LIMPIAR"
	echo "$AZUL = Ingrese el nombre del usuario en donde se ejecutaran los programas  = $LIMPIAR"
	echo "$AZUL ======================================================================= $LIMPIAR"
	echo
	read usuario

	echo
	echo "$AZUL Instalar wget $LIMPIAR"
	echo
	$GESTOR install -y wget

	echo
	echo "$AZUL Instalar gcc $LIMPIAR"
	echo
	$GESTOR install -y gcc

	echo
	echo "$AZUL Instalar gfortran $LIMPIAR"
	echo
	$GESTOR install -y gcc-gfortran

	echo
	echo "$AZUL Instalar BLAS $LIMPIAR"
	echo
	$GESTOR install -y blas blas-devel

	echo
	echo "$AZUL Instalar LAPACK $LIMPIAR"
	echo
	$GESTOR install -y lapack lapack-devel

	echo
	echo "$AZUL Intalar fftw $LIMPIAR"
	echo
	$GESTOR install -y fftw fftw-devel
	echo

	echo "$AZUL Instalar openmpi $LIMPIAR"
	echo
	$GESTOR install -y openmpi openmpi-devel
	echo '#OpenMPI: Inicio'>> /home/$usuario/.bashrc
	echo 'export PATH=/usr/lib64/openmpi/bin:$PATH' >> /home/$usuario/.bashrc
	echo 'export LD_LIBRARY_PATH=/usr/lib64/openmpi/lib:$LD_LIBRARY_PATH' >> /home/$usuario/.bashrc 
	echo '#OpenMPI: Fin' >> /home/$usuario/.bashrc
	source /home/$usuario/.bashrc

	echo
	echo "$AZUL Instalar openblas $LIMPIAR"
	echo
	$GESTOR install -y openblas openblas-devel

	echo
	echo "$AZUL Inslatar scalapack $LIMPIAR"
	echo
	$GESTOR install -y scalapack-openmpi scalapack-openmpi-devel

	echo
	echo "$AZUL Instalar gnuplot $LIMPIAR"
	echo
	$GESTOR install -y gnuplot

	echo
	echo "$AZUL Instalar Quantum-ESPRESSO $LIMPIAR"
	echo
	$GESTOR install -y quantum-espresso-openmpi quantum-espresso-openmpi-devel

	echo
	echo "$AZUL Instalar octave $LIMPIAR"
	echo
	$GESTOR install -y octave octave-devel

	echo
	echo "$AZUL Instalar nano $LIMPIAR"
	echo
	$GESTOR install -y nano

	echo
	echo "$AZUL Instalar tmux $LIMPIAR"
	echo
	$GESTOR install -y tmux

	echo
	echo "$AZUL Instalar XCrySDen $LIMPIAR"
	echo
	$GESTOR install -y libXScrnSaver
	wget www.xcrysden.org/download/xcrysden-1.5.60-linux_x86_64-semishared.tar.gz
	mv xcrysden-1.5.60-linux_x86_64-semishared.tar.gz /home/$usuario && cd /home/$usuario
	tar -zxvf xcrysden-1.5.60-linux_x86_64-semishared.tar.gz
	mv xcrysden-1.5.60-bin-semishared .xcrysden-1.5.60-bin-semishared
	echo "#xcrysden: Inicio" >> /home/$usuario/.bashrc
	echo "TCL_LIBRARY=/home/$usuario/.xcrysden-1.5.60-bin-semishared/external/lib/tcl8.5" >> /home/$usuario/.bashrc
	echo "XCRYSDEN_TOPDIR=/home/$usuario/.xcrysden-1.5.60-bin-semishared" >> /home/$usuario/.bashrc
	echo "XCRYSDEN_SCRATCH=/tmp" >> /home/$usuario/.bashrc
	echo "export XCRYSDEN_TOPDIR XCRYSDEN_SCRATCH" >> /home/$usuario/.bashrc
	echo 'PATH="$XCRYSDEN_TOPDIR:$XCRYSDEN_TOPDIR/scripts:$XCRYSDEN_TOPDIR/util:${PATH}"' >> /home/$usuario/.bashrc
	echo "#xcrysden: Fin" >> /home/$usuario/.bashrc
	source .bashrc
	rm xcrysden-1.5.60-linux_x86_64-semishared.tar.gz
	cd -
}

echo
echo "$VERDE Instalar programas usados por CMT $LIMPIAR"
echo

echo
echo "$AZUL ======================================================================= $LIMPIAR"
echo "$AZUL =  Eliga el sistema operativo en el que va a instalar los programas   = $LIMPIAR"
echo "$AZUL ======================================================================= $LIMPIAR"
echo "$LILA Digite:         1 = Fedora,    2 = CentOS,    3 = Ubtunu,    4 = Debian $LIMPIAR"
echo "$AZUL ======================================================================= $LIMPIAR"
echo "$ROJO Nota: $LIMPIAR Verifiqué que el paquete de interface grafica esté instalado" 
echo "$LIMPIAR y configurado para que se pueda ejecutar XCrySDen apropiadamente"        
echo
read OS
case $OS in
	1)
		echo "$AZUL Fedora $LIMPIAR"
		GESTOR=dnf
		redhat
 	;;
	2)
		echo "$AZUL CentOS $LIMPIAR"
		GESTOR=yum
		echo "$AZUL Instalar perl $LIMPIAR"
		$GESTOR install -y perl
		perl -pi -e "s/enabled=0/enabled=1/" /etc/yum.repos.d/CentOS-Base.repo
		echo
		echo "$AZUL Instalar EPEL $LIMPIAR"
		echo
		$GESTOR install -y epel-release
		redhat
		perl -pi -e "s/enabled=1/enabled=0/" /etc/yum.repos.d/CentOS-Base.repo
	;;
	3)
		echo "$AZUL Ubuntu $LIMPIAR"
		GESTOR="sudo apt-get"
		debian
	;;
	4)
		echo "$AZUL Debian $LIMPIAR"
		GESTOR=apt-get
		debian
	;;
esac

echo
echo "$VERDE Fin de la instalación $LIMPIAR"
echo

exit 0
